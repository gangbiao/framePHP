<?php
//框架默认配置项
return array(
	'TIME_ZONE' => 'PRC',
	//验证码配置项***********************
	//验证码长度
	'CODE_LEN'	=>	1,
	'CODE_SIZE'	=>	12,
	'CODE_WIDTH'=> 65,
	'CODE_HEIGHT'=> 25,
	'CODE_COLOR' => '#000000',
	'CODE_BGCOLOR'=>'#ffffff',
	'CODE_SEED' =>	'zxcvbnmnasdfghjklqwertyuiop1234567890',
	'CODE_FONTFILE'=> FONT_PATH . '/font.ttf',
	//上传配置*********************************
	'UPLOAD_TYPE'=>array('jpg','jpeg','png','gif'),
	//上传图片大小
	'UPLOAD_SIZE' => 3000000,
	//上传路径
	'UPLOAD_PATH' => './upload/' . date('ymd'),
	//缩略图配置*************************
	'THUMB_HEIGHT'	=>	300,
	'THUMB_WIDTH'	=>	500,
	'THUMB_SUFFIX'	=>	'_thumb',
	//水印配置****************************
	'WATER_PCT'		=>	80,
	'WATER_POSITION'=> 20,
	'WATER_SUFFIX'	=>	'_water',
	//session配置
	'SESSION_START'=> true,
	//数据库配置项
	//
	'DB_HOST' => 'localhost',
	'DB_USER' => 'root',
	'DB_PWD' => '',
	'DB_NAME' =>'',
	'DB_FREFIX' =>'',
	'DB_CHARSET' => 'UTF8',
	'RIGHT_DELIMITER' => '}',
	'LEFT_DELIMITER' =>'{GB'
	);
	?>
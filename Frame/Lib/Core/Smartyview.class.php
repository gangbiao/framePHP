<?php 
CLASS Smartyview{
	private static $smarty; 
	public function __construct(){
		if(!is_null(self::$smarty)) return;

		$smarty = new Smarty();
		//模版目录
		$smarty->template_dir = APP_TPL_PATH . '/' . CONTROL;
		//编译目录
		$smarty->compile_dir = APP_COMPILE_PATH;
		 //缓存目录
		$smarty->cache_dir = APP_CACHE_PATH;
		 //开始定界符
		$smarty->left_delimiter = C('LEFT_DELIMITER');
		 //结束定界符
		$smarty->right_delimiter = C('RIGHT_DELIMITER');
		//保存到属性中
		self::$smarty = $smarty;
	}

	public function display($tpl = NULL){
		if(is_null($tpl)){
			$path = METHOD . '.html';

		}else{
			$type = ltrim(strrchr($tpl, '.'),'.');
			if(empty($type)) $tpl .= '.html';
			$path = $tpl;
		}
		$fullPath = APP_TPL_PATH . '/' . CONTROL . '/' . $path;
		if(!is_file($fullPath)) halt('模板：' . $fullPath . '未找到！):');

		self::$smarty->display($fullPath);
	}

		/**
	 * [assign 分配变量]
	 * @param  [type] $var   [description]
	 * @param  [type] $value [description]
	 * @return [type]        [description]
	 */
	protected function assign($var,$value){
		//调用smarty的assign方法
		self::$smarty->assign($var,$value);
	}


}



 ?>
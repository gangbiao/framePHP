<?php 
	class Adhibition extends Action{
		public static function apply(){
			//初始化框架
			self::_initial_frame();

			//设置外部链接
			self::_set_url();
			
			//自动载入
			//给当前类创建一个_autoload方法
			spl_autoload_register(array(__CLASS__,"_autoload"));

			//建立deom()  一个默认的控制器
			self::_create_demo();
			//实例话控制器，输出欢迎语
			self::_app_original();
		}
		//初始化框架
		private static function _initial_frame(){
			//用C函数读取框架的配置文件
			C(include CONFIG_PATH.'/Config.php');
			//首先声明 用户的配置文件路径
			$app_config = APP_CONFIG_PATH.'/Config.php';
			//然后判断如果不是文件的话，就创建文件往里面写
			if(!is_file($app_config)){
			$data=<<<str
<?php 
return array(
	//'配置项' => '配置值'
		);

str;
		//写入文件，数据为$data||如果创建不成功，就终止程序
		file_put_contents($app_config,$data)||die('access not allow');
	}
	//有用户的配置文件的时候，就用C函数读
	C(include $app_config);
	//设置时区
	date_default_timezone_set(C('TIME_ZONE'));
	//开启session
		C('SESSION_START') && session_start();
		}

		//设置设置外部链接
		private static function _set_url(){
			//c代表类名，如果有传参的话，就等于c, 如果没有就默认是Index
		$control = isset($_GET['c'])? htmlspecialchars($_GET['c']):'Index';
		
		//m代表方法名，如果GET传参的话，就让它等于m  ，如果不是的话，就默认等于Index
		$method = isset($_GET['m'])? htmlspecialchars($_GET['m']):'index';


		define('CONTROL',$control);
		define('METHOD',$method);
			//定一个常量__ROOT__ 值是  项目路径
		define('__ROOT__','http://'.$_SERVER['HTTP_HOST'].$_SERVER['SCRIPT_NAME']);
		define('__TPL__', dirname(__ROOT__) . '/' . APP_NAME . '/Tpl');
		define('__PUBLIC__', __TPL__ . '/Public');
		define('__SELF__', 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']);
		define('__INDEX__',dirname(__ROOT__));
		}

		//自动载入
		//当引用一个类的时候会自动执行这个函数
		private static function _autoload($className){
			//引用一个类时自劢执行的函数，可以读入类文
			//判断这个传入的类名是否大于7  并且 这个类名是否以Action 结尾 。
			if(strlen($className) > 6 && substr($className, -6) == 'Action'){
				//这是引用控制器夹下的控制文件  
				$path = APP_CONTROL_PATH.'/'.$className.'.class.php';
				//在引用控制器文件时，不存在使用halt函数  提示。并且终止程序
				if(!is_file($path)) halt('控制器：'. $path . '不存在');
			}else{
				//否则就去工具文件找。
				$path = TOOL_PATH . '/' . $className . '.class.php';
				//在引用工具文件时，不存在使用halt函数  提示。并且终止程序
				if(!is_file($path)) halt('工具类：'. $path . '不存在');
			}
			require $path;
		}
		//建立一个demo,  给用户一个默认的控制器
		private static function _create_demo(){
			//默认控制区默认文件
			$app_control = APP_CONTROL_PATH.'/IndexAction.class.php';
			//判断如果不是一个文件的时候，让他/她写入，创建
			if (!is_file($app_control)) {
				$data = <<<str
<?php
// 本类由系统自动生成，仅供测试用途
header('Content-type:text/html;charset=utf-8');
class IndexAction extends Action {
    public function index(){
	 echo '<style type="text/css">*{ padding: 0; margin: 0; } div{ padding: 4px 48px;} body{ background: #fff; font-family: "微软雅黑"; color: #333;} h1{ font-size: 100px; font-weight: normal; margin-bottom: 12px; } p{ line-height: 1.8em; font-size: 36px }</style><div style="padding: 24px 48px;"> <h1>:)</h1><p>欢迎使用 <b>FramePHP</b>！</p></div><script type="text/javascript" src="http://tajs.qq.com/stats?sId=9347272" charset="UTF-8"></script>';
    }
}
str;
		//写入创建文件，如果不成功的话，就让它终止程序。
		file_put_contents($app_control, $data)|| die('access not allow');
			}
		}
		// 实例话控制器，输出欢迎语
		public static function _app_original(){
		


		//leiming并且要连上Action   如：IndexAction
		$control = CONTROL.'Action';
		//实例化一个类，默认是IndexAction,所以他通过自动加载方法  ，引用了IndexAction.class.php 这个文件
		
		$obj = new $control;
		//执行对像里面的方法。默认是index()
		$method = METHOD;
		$obj->$method();
		}
	}
?>
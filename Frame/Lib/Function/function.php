<?php
/**
 * [***********************]
 * @Author: mazhenyu[houdunwangmzy@163.com]
 * @Date:   2014-08-06 17:11:08
 * @Last Modified by:   nic-tes
 * @Last Modified time: 2014-08-20 15:08:58
 */
//停止
//
function M($table){
$obj = new Model($table);
return $obj;
}
function halt($msg){
	header('Content-type:text/html;charset=utf-8');
	echo '<h2 style="margin-top:50px;margin-left:50px;">' . $msg . '</h2>';die;
}
function dump($var){
	var_dump($var);
}

function p($var){
	echo '<pre>';
	print_r($var);
	echo '</pre>';
}
//往指定链接跳转
function go($url){
	header('Location:' . $url);
	die;
}

/**
 * [C 读取或者临时动态设置配置项]
 * @param [type] $var   [description]
 * @param [type] $value [description]
 */
function C($var=NULL,$value=NULL){
	static $config = array();
	//第一个参数是数组的时候，要把配置项加载的静态变量里面
	if(is_array($var)){
		$config = array_merge($config,$var);
		return;
	}
	//第一个参数是字符串(第二个参数有两种情况)
	if(is_string($var)){
		// 第二个参数第一种情况,C('CODE_LEN')，调用配置
		if(is_null($value)){
			return isset($config[$var]) ? $config[$var] : NULL;
		}else{// 第二个参数第二种情况,C('CODE_LEN',1)
			$config[$var] = $value;
			return;
		}
	}
	//什么参数也不传,返回所有的配置项
	if(is_null($var)){
		return $config;
	}
}

function adds_html($str){
	return htmlspecialchars(addslashes($str));
}

function success($msg,$url){
	header('Content-type:text/html;charset=utf-8');
	echo "<script>alert('$msg');location.href='$url'</script>";
	die;
}

function error($msg){
	header('Content-type:text/html;charset=utf-8');
	echo "<script>alert('$msg');window.history.back()</script>";
	die;
}

/**
 * [data_to_file 把数组写入到文件]
 * @param  [type] $db     [description]
 * @param  [type] $dbfile [description]
 * @return [type]         [description]
 */
function data_to_file($dbfile,$db){
	$data = var_export($db,true);
	$str = <<<str
<?php
return {$data};
?>
str;
	//写入数据库
	file_put_contents($dbfile, $str);
	
}

/**
 * [get_size 格式化容量]
 * @param  [type] $total [description]
 * @return [type]        [description]
 */
function get_size($total){

	switch (true) {
		case $total > pow(1024, 3):
			$unit = array(3,'G');
			break;
		case $total > pow(1024, 2):
			$unit = array(2,'M');
			break;
		case $total > pow(1024, 1):
			$unit = array(1,'KB');
			break;
		
		default:
			$unit = array(0,'B');
			break;
	}

	return round($total/pow(1024,$unit[0]),2) . $unit[1];

}
function del($dirName) {
	//统一路径
	$dirName = change_path($dirName);
	if (!is_dir($dirName)) return false;
	foreach (glob($dirName. "/*") as $v){
		is_dir($v) ? del($v) : unlink($v);
	}
	return rmdir($dirName);

}

/**
 * [move 移动函数]
 * @param  [type] $source [description]
 * @param  [type] $dest   [description]
 * @return [type]         [description]
 */
function move($source,$dest){
	cp($source,$dest);
	del($source);
}

/**
 * [cp 复制文件夹]
 * @param  [type] $source [description]
 * @param  [type] $dest   [description]
 * @return [type]         [description]
 */
function cp($source,$dest){
	//统一路径
	$source = change_path($source);
	$dest = change_path($dest);
	//源路径不存在则返回假
	if(!is_dir($source)) return false;
	//判断目标路径是否存在
	if(!is_dir($dest)) mkdir($dest,0777,true);
	//循环源目录组合新路径
	foreach (glob($source . '/*') as $v) {
		$newDest = $dest . '/' . basename($v);
		//如果源目录是文件夹则递归，否则复制文件
		is_dir($v) ? cp($v,$newDest) : copy($v, $newDest);
	}

}

/**
 * [change_path 统一路径，保证windows与linux都可以使用]
 * @param  [type] $path [description]
 * @return [type]       [description]
 */
function change_path($path){
	$path = str_replace('\\', '/', $path);
	return rtrim($path,'/');
}

/**
 * [get_dir_size 获得文件夹大小]
 * @param  [type] $path [description]
 * @return [type]       [description]
 */
function get_dir_size($path){
	$path = change_path($path);
	static $size = 0;
	foreach (glob($path . '/*') as $v) {
		is_dir($v) ? get_dir_size($v) : $size += filesize($v);
	}
	return $size;
}

// function get_dir_size($path){
// 	$path = change_path($path);
// 	$size = 0;
// 	foreach (glob($path . '/*') as $v) {
// 		$size += is_dir($v) ? get_dir_size($v) : filesize($v);
// 	}
// 	return $size;
// }

function upload($path){
	//重组数组
	$arr = reset_arr();
	//上传
	move_upload($path,$arr);

}

/**
 * [move_upload 上传]
 * @param  [type] $path [description]
 * @param  [type] $arr  [description]
 * @return [type]       [description]
 */
function move_upload($path,$arr){
	foreach ($arr as $v) {
		if(is_uploaded_file($v['tmp_name'])){
			//取得扩展名
			$info = pathinfo($v['name']);
			$type = $info['extension'];
			$filename = time() . mt_rand(0,9999) . '.' . $type;
			//处理上传路径
			$path = change_path($path);
			is_dir($path) || mkdir($path,0777,true);
			//完整路径
			$fullPath = $path . '/' . $filename;
			move_uploaded_file($v['tmp_name'],$fullPath);
			//取得扩展名, destination)
		}
	}
}

function reset_arr(){
	$temp = array();
	foreach ($_FILES as $v) {
		//$v里面的name是数组则是多张上传
		if(is_array($v['name'])){
			foreach ($v['name'] as $key => $value) {
				$temp[] = array(
					'name'	    =>	$value,
					'type'	    =>	$v['type'][$key],
					'tmp_name'	=>	$v['tmp_name'][$key],
					'error'	    =>	$v['error'][$key],
					'size'	    =>	$v['size'][$key],
					);
			}
		}else{//单张上传
			$temp[] = $v;
		}
	}

	return $temp;
}



function print_const(){
	$const = get_defined_constants(true);
	P($const['user']);
}
/**
 * [__autoload 找不到类会自动载入]
 * @param  [type] $className [description]
 * @return [type]            [description]
 */
// function __autoload($className){
// 	include "./" . $className . '.class.php';
// }


?>
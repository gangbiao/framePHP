<?php

function smarty_modifier_truncate_d($string, $length = 5, $etc = '...'){
	return mb_substr($string, 0,$length,'utf-8') . $etc;
}

/* vim: set expandtab: */

?>

<?php
/**
 * @Author: nic-tes
 * @Date:   2014-08-18 17:51:26
 * @Last Modified by:   nic-tes
 * @Last Modified time: 2014-08-19 16:23:40
 */
class Upload{
	//上传路径
	private $path;
	// 上传图片类型
	private $type;
	// 上传图片大小
	private $size;
	//保存错误信息
	public $error;

	/**
	 * [__construct 构造函数初始化]
	 */
	public function __construct($path = NULL, $type = NULL, $size = NULL){
		$this->path = is_null($path) ? C('UPLOAD_PATH') : $path;
		$this->type = is_null($type) ? C('UPLOAD_TYPE') : $type;
		$this->size = is_null($size) ? C('UPLOAD_SIZE') : $size;
	}
	/**
	 * [up 上传]
	 * @return [type] [description]
	 */
	public function up(){
		//1.重组数组
		$arr = $this->_reset_arr();
		//临时数组(保存上传文件的信息)
		$tempArr = array();
		//2.筛选
		foreach ($arr as $v) {
			//筛选合格的可以上传的
			if($this->_filter($v)){
				//3.合格的上传
				$tempArr[] = $this->_move($v);
			}
		}
		// 4.反出上传信息
		return $tempArr;
	
	}

	/**
	 * [_move 上传]
	 * @param  [type] $v [description]
	 * @return [type]    [description]
	 */
	private function _move($v){
		//取得扩展名
		$info = pathinfo($v['name']);
		$type = $info['extension'];
		$filename = time() . mt_rand(0,9999) . '.' . $type;
		//处理上传路径
		$path = str_replace('\\', '/', $this->path);
		$path = rtrim($path,'/');
		is_dir($path) || mkdir($path,0777,true);

		//完整路径
		$fullPath = $path . '/' . $filename;
		move_uploaded_file($v['tmp_name'],$fullPath);
		
		//给$v压入完整路径
		$v['upload'] = $fullPath;
		return $v;
	}

	/**
	 * [_filter 筛选]
	 * @param  [type] $v [description]
	 * @return [type]    [description]
	 */
	private function _filter($v){
		//获得图片类型
		$info = pathinfo($v['name']);
		$type = isset($info['extension']) ? $info['extension'] : '';
		//判断各种情况
		switch (true) {
			//1.不是一个合法的上传文件
			case !is_uploaded_file($v['tmp_name']):
				$this->error = '不是一个合法的上传文件';
				return false;
			//2.类型是否允许
			case !in_array($type, $this->type):
				$this->error = '上传类型不允许';
				return false;
			//3.大小是否允许
			case $v['size'] > $this->size:
				$this->error = '上传文件大小超出网站配置';
				return false;
			//4.大小超过了 php.ini 中 upload_max_filesize 限制值
			case $v['error'] == 1:
				$this->error = '大小超过了 php.ini 中 upload_max_filesize 限制值';
				return false;
			//5.大小超过 HTML 表单中 MAX_FILE_SIZE 选项指定的值
			case $v['error'] == 2:
				$this->error = '大小超过 HTML 表单中 MAX_FILE_SIZE 选项指定的值';
				return false;
			//6.文件只有部分被上传
			case $v['error'] == 3:
				$this->error = '文件只有部分被上传';
				return false;
			//7.没有文件被上传
			case $v['error'] == 4:
				$this->error = '没有文件被上传';
				return false;
			default:
				return true;
		}
	}

	/**
	 * [_reset_arr 重组数组]
	 * @return [type] [description]
	 */
	private function _reset_arr(){
		$temp = array();
		foreach ($_FILES as $v) {
			//$v里面的name是数组则是多张上传
			if(is_array($v['name'])){
				foreach ($v['name'] as $key => $value) {
					$temp[] = array(
						'name'	    =>	$value,
						'type'	    =>	$v['type'][$key],
						'tmp_name'	=>	$v['tmp_name'][$key],
						'error'	    =>	$v['error'][$key],
						'size'	    =>	$v['size'][$key],
						);
				}
			}else{//单张上传
				$temp[] = $v;
			}
		}

		return $temp;
	}
}
?>